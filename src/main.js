import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'

const axios = require('axios');
window.axios = axios

window.$ = require('jquery')
require('popper.js')
require('bootstrap')

window.Vue = Vue
Vue.config.productionTip = false
var _ = require('lodash')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
