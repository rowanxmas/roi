export default {
	data : function () {
			return {
				id : ''
			}
	},
	computed : {
		selector : function () {
			return '#'+this.id;
		}
	},
	methods: {
		guid  () {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
		}	
	},
	mounted : function () {
		this.id = 'a'+this.guid();
	}
}